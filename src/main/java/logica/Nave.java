package logica;

public class Nave extends Vehiculo{
    
    //Atrubutos
    private int marineros;
    private boolean estado;
    private double daño_Acumulado;
    private String simbologia;
    
    //Constructor

    public Nave() {
        super();
    }

    
    public Nave( String clase_navio, String tipo_municion, int litros_Combustibles, int marineros, boolean estado, double daño_Acumulado, String simbologia) {
        super(clase_navio, tipo_municion, litros_Combustibles);
        this.marineros = marineros;
        this.estado = estado;
        this.daño_Acumulado = daño_Acumulado;
        this.simbologia = simbologia;
    }
    
    
    //Metodos

    public int getMarineros() {
        return marineros;
    }

    public void setMarineros(int marineros) {
        this.marineros = marineros;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public double getDaño_Acumulado() {
        return daño_Acumulado;
    }

    public void setDaño_Acumulado(double daño_Acumulado) {
        this.daño_Acumulado = daño_Acumulado;
    }

    public String getSimbologia() {
        return simbologia;
    }

    public void setSimbologia(String simbologia) {
        this.simbologia = simbologia;
    }
    
    
    
    
    
    
    
    

}
