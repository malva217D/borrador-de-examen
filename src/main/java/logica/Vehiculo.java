package logica;

public abstract class Vehiculo {
    //Atributos
    protected String clase_navio;
    protected String tipo_municion;
    protected int litros_Combustibles;
    
    //Constructor

    public Vehiculo() {
    }

    public Vehiculo(String clase_navio, String tipo_municion, int litros_Combustibles) {
        this.clase_navio = clase_navio;
        this.tipo_municion = tipo_municion;
        this.litros_Combustibles = litros_Combustibles;
    }
    
    //Metodos

    public String getClase_navio() {
        return clase_navio;
    }

    public void setClase_navio(String clase_navio) {
        this.clase_navio = clase_navio;
    }

    public String getTipo_municion() {
        return tipo_municion;
    }

    public void setTipo_municion(String tipo_municion) {
        this.tipo_municion = tipo_municion;
    }

    public int getLitros_Combustibles() {
        return litros_Combustibles;
    }

    public void setLitros_Combustibles(int litros_Combustibles) {
        this.litros_Combustibles = litros_Combustibles;
    }
    
    
    
    

}
