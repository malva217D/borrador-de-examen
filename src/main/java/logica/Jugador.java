package logica;

import java.util.Arrays;
import java.util.List;

public class Jugador extends Persona {

    //Atributos
    private String usuario;
    private String fecha_Ingreso;
    Tablero tablero;
    int modo;
    private List<String> naves_disponibles = Arrays.asList(
            "destructor", "fragata", "submarino", "explorador", "portaviones");

    //Constructor
    public Jugador() {
        super();
    }

    public Jugador(int codigo, String nombre, String nacionalidad, String usuario, String fecha_Ingreso, Tablero tablero, int modo) {
        super(codigo, nombre, nacionalidad);
        this.usuario = usuario;
        this.fecha_Ingreso = fecha_Ingreso;
        this.tablero = tablero;
        this.modo = modo;
    }

    //Metodos
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFecha_Ingreso() {
        return fecha_Ingreso;
    }

    public void setFecha_Ingreso(String fecha_Ingreso) {
        this.fecha_Ingreso = fecha_Ingreso;
    }

    public Tablero llenarTableroManual(Tablero tablero) {

        return tablero;
    }

    public Tablero llenarTableroAutomatico(Tablero tablero) {
        int columna = 0;
        int fila = 0;
        int opcion = 0;
        String clase = "";
        String municion = "";
        Nave nave;
        for (int i = 0; i < 10; i++) {
            do {
                columna = (int) (Math.random() * 5);
                fila = (int) (Math.random() * 5);
            } while (tablero.matriz[fila][columna] != null);
            opcion = (int) (Math.random() * 5);
            clase = naves_disponibles.get(opcion);
            //municion = this.obtenerMunicion(clase);
            nave = new Nave(clase, "Balas", 1000, 20, true, 100, clase);
            tablero.matriz[fila][columna] = nave;
        }

        return tablero;
    }

    public void jugar() {

    }

    public void rendirse() {

    }

    public Tablero monstrarTablero() {
        return null;
    }

}
