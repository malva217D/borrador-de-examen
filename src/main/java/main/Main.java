package main;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;
import logica.Jugador;
import logica.Tablero;

public class Main {

    public static void main(String[] args) throws ParseException {

        ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
        int nJugadores;
        int modo;
        int nFilas, nColumnas;
        Scanner sc = new Scanner(System.in);
        System.out.print("Cuantos Jugadores va ingresar:");
        nJugadores = sc.nextInt();
        System.out.print("Ingrese la Cantidad de Filas del Tablero");
        nFilas = sc.nextInt();
        System.out.print("Ingrese la Cantidad de Columnas del Tablero");
        nColumnas = sc.nextInt();
        for (int i = 0; i < nJugadores; i++) {
            System.out.println("Registro de Jugador " + i + 1);
            System.out.print("Ingrese el Codigo:");
            int codigo = sc.nextInt();
            System.out.println("Ingrese el nombre:");
            String usuario = sc.next();
            System.out.println("Ingrese la Nacionalidad:");
            String nacionalidad = sc.next();
            System.out.println("Ingrese el correo(dominio@utn.ac.cr):");
            String correo = sc.next();
            System.out.println("Ingrese la Fecha Ingreso:");
            String fecha_Ingreso = sc.next();
            System.out.println("Como deseas Cargar el Tablero"
                    + "1-Manual"
                    + "2-Automatico"
                    + "Selecion: ");
            modo = sc.nextInt();
            Tablero tablero = new Tablero(nFilas, nColumnas);
            Jugador jugador = new Jugador(codigo, correo, nacionalidad, usuario, fecha_Ingreso, tablero, modo);
            if (modo == 1) {
                jugador.llenarTableroManual(tablero);

            } else {
                jugador.llenarTableroAutomatico(tablero);
            }

            System.out.println("----------------------------------------------------------------------");
            jugadores.add(jugador);

        }

        //Recordar Mover esto de aqui Abajo
    }
}
